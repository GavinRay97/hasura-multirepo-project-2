const mainContentEl = document.querySelector('#main-content')

async function main() {
  const query = `
    query {
      blogposts {
        id
        title
        content
      }
    }
  `
  const req = await fetch('http://localhost:8090/v1/graphql', {
    method: 'POST',
    body: JSON.stringify({ query }),
  })

  const res = await req.json()
  const { blogposts } = res.data

  const makeListItem = (blogpost) =>
    `<li>[${blogpost.id}]: ${blogpost.title} ${blogpost.content}</li>`

  const items = blogposts.map(makeListItem).join('')
  if (mainContentEl) mainContentEl.innerHTML = items
}

main()
